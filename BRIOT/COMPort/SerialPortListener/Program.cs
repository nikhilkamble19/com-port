﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace SerialPortListener
{
    static class Program
    {
        [DllImport("user32.dll")]
        private static extern
        bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern
            bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll")]
        private static extern
            bool IsIconic(IntPtr hWnd);

        private const int SW_RESTORE = 9;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWNORMAL = 1;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                string strProcessName = Process.GetCurrentProcess().ProcessName;
                Process[] Oprocess = Process.GetProcessesByName(strProcessName);
                if (Oprocess.Length > 1)
                {
                    //MessageBox.Show("In If Loop");
                    Process p = Process.GetCurrentProcess();
                    int n = 0;        // assume the other process is at index 0
                                      // if this process id is OUR process ID...
                    if (Oprocess[0].Id == p.Id)
                    {
                        //MessageBox.Show(p.Id.ToString());
                        // then the other process is at index 1
                        n = 1;
                    }
                    // get the window handle
                    IntPtr hWnd = Oprocess[n].MainWindowHandle;
                    // if iconic, we need to restore the window
                    if (IsIconic(hWnd))
                    {
                        //MessageBox.Show(hWnd.ToString());
                        ShowWindowAsync(hWnd, SW_SHOWNORMAL);
                    }
                    // bring it to the foreground
                    SetForegroundWindow(hWnd);
                    if (!SetForegroundWindow(hWnd))
                    {
                        ShowWindowAsync(hWnd, SW_SHOWMINIMIZED);
                        ShowWindowAsync(hWnd, SW_SHOWNORMAL);
                    }

                    // exit our process
                    //return;
                }
                else
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new MainForm());
                }
            }
            catch
            {

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Reflection;
using System.ComponentModel;
using System.Threading;
using System.IO;

namespace SerialPortListener.Serial
{
    /// <summary>
    /// Manager for serial port data
    /// </summary>
    public class SerialPortManager : IDisposable
    {
        int serialPortCount = 0;
        public SerialPortManager()
        {
            try {
                // Finding installed serial ports on hardware
                _currentSerialSettings.PortNameCollection = SerialPort.GetPortNames();
                _currentSerialSettings.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(_currentSerialSettings_PropertyChanged);

                // If serial ports is found, we select the first found
                if (_currentSerialSettings.PortNameCollection.Length > 0)
                    _currentSerialSettings.PortName = _currentSerialSettings.PortNameCollection[0];
            }
            catch
            {

            }
        }

        
        ~SerialPortManager()
        {
            Dispose(false);
        }


        #region Fields
        private SerialPort[] _serialPort = new SerialPort[50];
        private SerialSettings _currentSerialSettings = new SerialSettings();
        private string _latestRecieved = String.Empty;
        public event EventHandler<SerialDataEventArgs> NewSerialDataRecieved; 

        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the current serial port settings
        /// </summary>
        public SerialSettings CurrentSerialSettings
        {
            get { return _currentSerialSettings; }
            set { _currentSerialSettings = value; }
        }

        #endregion

        #region Event handlers

        void _currentSerialSettings_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            try
            {
                // if serial port is changed, a new baud query is issued
                if (e.PropertyName.Equals("PortName"))
                    UpdateBaudRateCollection();
            }
            catch
            {

            }
        }

        
        void _serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                byte[] data = new byte[5000];
                int dataLength;
                int nbrDataRead;
                for (int i = 0; i < serialPortCount; i++)
                {
                    dataLength = _serialPort[i].BytesToRead;
                    data = new byte[dataLength];
                    nbrDataRead = _serialPort[i].Read(data, 0, dataLength);
                    //if (nbrDataRead == 0)
                    //return;

                    // Send data to whom ever interested
                    if (NewSerialDataRecieved != null)
                    {
                        NewSerialDataRecieved(this, new SerialDataEventArgs(data));
                    }
                }
            }
            catch
            {

            }
        }

        
        #endregion

        #region Methods

        /// <summary>
        /// Connects to a serial port defined through the current settings
        /// </summary>
        public void StartListening(string[] portNameCollection)
        {
            try
            {
                int i = 0;
                foreach (string n in portNameCollection)
                {
                    if (!String.IsNullOrEmpty(n))
                    {
                        // Closing serial port if it is open
                        if (_serialPort[i] != null && _serialPort[i].IsOpen)
                            _serialPort[i].Close();

                        // Setting serial port settings
                        _serialPort[i] = new SerialPort(
                            n,
                            _currentSerialSettings.BaudRate,
                            _currentSerialSettings.Parity,
                            _currentSerialSettings.DataBits,
                            _currentSerialSettings.StopBits);

                        // Subscribe to event and open serial port for data
                        _serialPort[i].DataReceived += new SerialDataReceivedEventHandler(_serialPort_DataReceived);
                        _serialPort[i].Open();
                        i++;
                        serialPortCount++;
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Closes the serial port
        /// </summary>
        public void StopListening()
        {
            try
            {
                for (int i = 0; i < serialPortCount; i++)
                {
                    _serialPort[i].Close();
                }
            }
            catch
            {

            }
        }


        /// <summary>
        /// Retrieves the current selected device's COMMPROP structure, and extracts the dwSettableBaud property
        /// </summary>
        private void UpdateBaudRateCollection()
        {
            try
            {
                for (int i = 0; i < serialPortCount; i++)
                {
                    _serialPort[i] = new SerialPort(_currentSerialSettings.PortName);
                    _serialPort[i].Open();
                    object p = _serialPort[i].BaseStream.GetType().GetField("commProp", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_serialPort[i].BaseStream);
                    Int32 dwSettableBaud = (Int32)p.GetType().GetField("dwSettableBaud", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public).GetValue(p);

                    _serialPort[i].Close();
                    _currentSerialSettings.UpdateBaudRateCollection(dwSettableBaud);
                }
            }
            catch
            {

            }
        }

        // Call to release serial port
        public void Dispose()
        {
            Dispose(true);
        }

        // Part of basic design pattern for implementing Dispose
        protected virtual void Dispose(bool disposing)
        {
            try
            {
                for (int i = 0; i < serialPortCount; i++)
                {
                    if (disposing)
                    {
                        _serialPort[i].DataReceived -= new SerialDataReceivedEventHandler(_serialPort_DataReceived);
                    }
                    // Releasing serial port (and other unmanaged objects)
                    if (_serialPort[i] != null)
                    {
                        if (_serialPort[i].IsOpen)
                            _serialPort[i].Close();

                        _serialPort[i].Dispose();
                    }
                }
            }
            catch
            {

            }
        }


        #endregion

    }

    /// <summary>
    /// EventArgs used to send bytes recieved on serial port
    /// </summary>
    public class SerialDataEventArgs : EventArgs
    {
        public SerialDataEventArgs(byte[] dataInByteArray)
        {
            try
            {
                Data = dataInByteArray;
            }
            catch
            {

            }
        }

        /// <summary>
        /// Byte array containing data from serial port
        /// </summary>
        public byte[] Data;
    }
}

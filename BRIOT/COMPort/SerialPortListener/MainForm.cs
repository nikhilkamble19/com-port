﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SerialPortListener.Serial;
using System.IO;
using System.Diagnostics;
using MySql.Data.MySqlClient;

namespace SerialPortListener
{
    public partial class MainForm : Form
    {
        string localfileDB;
        string serverfileDB;
        //string copyServerfileDB;
        string str;
        string checkString;
        DateTime newDatetime = new DateTime();
        string currentDate;
        string[] portNameCollection = new string[300];
        string[] serverlines = new string[500];
        string[] locallines = new string[500];
        SerialPortManager _spManager;
        bool filePresentFlag = false;
        Timer timer = new Timer();
        MySqlConnection con = new MySqlConnection(Properties.Settings.Default.con);

        public MainForm()
        {
            try
            {
                InitializeComponent();

                UserInitialization();
            }
            catch
            {

            }
        }


        private void UserInitialization()
        {
            try
            {
                _spManager = new SerialPortManager();
                SerialSettings mySerialSettings = _spManager.CurrentSerialSettings;
                serialSettingsBindingSource.DataSource = mySerialSettings;
                portNameComboBox.DataSource = mySerialSettings.PortNameCollection;
                baudRateComboBox.DataSource = mySerialSettings.BaudRateCollection;
                dataBitsComboBox.DataSource = mySerialSettings.DataBitsCollection;
                parityComboBox.DataSource = Enum.GetValues(typeof(System.IO.Ports.Parity));
                stopBitsComboBox.DataSource = Enum.GetValues(typeof(System.IO.Ports.StopBits));
                //MessageBox.Show(portNameComboBox.Items.Count.ToString());
                if (portNameComboBox.Items.Count == 0)
                {
                    MessageBox.Show("No Scanner at COM Port, Please connect a scanner to Port and Restart the Software");
                    this.Close();
                }
                else
                {
                    for (int i = 0; i < portNameComboBox.Items.Count; i++)
                    {
                        portNameCollection[i] = portNameComboBox.GetItemText(portNameComboBox.Items[i]);
                    }
                }
                _spManager.NewSerialDataRecieved += new EventHandler<SerialDataEventArgs>(_spManager_NewSerialDataRecieved);
                this.FormClosing += new FormClosingEventHandler(MainForm_FormClosing);
            }
            catch
            {
                //MessageBox.Show(ex.ToString());
            }
        }


        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                _spManager.Dispose();
            }
            catch
            {

            }
        }

        void _spManager_NewSerialDataRecieved(object sender, SerialDataEventArgs e)
        {
            try
            {
                if (this.InvokeRequired)
                {
                    // Using this.Invoke causes deadlock when closing serial port, and BeginInvoke is good practice anyway.
                    this.BeginInvoke(new EventHandler<SerialDataEventArgs>(_spManager_NewSerialDataRecieved), new object[] { sender, e });
                    return;
                }

                int maxTextLength = 1000; // maximum text length in text box
                if (tbData.TextLength > maxTextLength)
                    tbData.Text = tbData.Text.Remove(0, tbData.TextLength - maxTextLength);

                // This application is connected to a GPS sending ASCCI characters, so data is converted to text
                str = Encoding.ASCII.GetString(e.Data);
                checkString = str;
                if (!String.IsNullOrEmpty(str))
                {
                    try
                    {
                        //bool flag = true;
                        newDatetime = DateTime.Now;
                        str = str + newDatetime.ToString("#ddMMyyyy#HHmmss");
                        con.Close();
                        con.Open();
                        string q = "SELECT * FROM master where checkString = '" + checkString + "'";
                        MySqlCommand command = new MySqlCommand(q, con);
                        MySqlDataReader mdr = command.ExecuteReader();
                        if (mdr.Read())
                        {

                        }
                        else
                        {
                            mdr.Close();
                            MySqlCommand cmd = con.CreateCommand();
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = "insert into master values ('"+checkString+"','"+str+"','"+DateTime.Now.ToString("yyyy-MM-dd")+"')";
                            cmd.ExecuteNonQuery();
                        }
                        con.Close();
                        //MessageBox.Show(localfileDB, @"C:\Users\karan\Desktop\COMPORT Project\Output.txt");
                        /*string[] lines = System.IO.File.ReadAllLines(serverfileDB);

                        foreach (string line in lines)
                        {
                            if (line.Contains(checkString))
                            {
                                flag = false;
                                break;
                            }
                            else
                            {
                                flag = true;
                            }
                        }

                        if (flag == true)
                        {
                            WriteonTOP();
                            tbData.AppendText(str);
                            tbData.ScrollToCaret();
                        }*/
                    }
                    catch
                    {
                        //MessageBox.Show(ex.ToString());
                    }
                }
            }
            catch
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        // Handles the "Start Listening"-buttom click event
        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                _spManager.StartListening(portNameCollection);
            }
            catch
            {

            }
        }

        // Handles the "Stop Listening"-buttom click event
        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                _spManager.StopListening();
            }
            catch
            {

            }
        }

        private void WriteonTOP()
        {
            try
            {
                /*string tempfile = Path.GetTempFileName();
                using (var writer = new StreamWriter(tempfile))
                using (var reader = new StreamReader(serverfileDB))
                {
                    writer.WriteLine(str);
                    while (!reader.EndOfStream)
                        writer.WriteLine(reader.ReadLine());
                }
                File.Copy(tempfile, localfileDB, true);
                File.Copy(tempfile, serverfileDB, true);*/

            }
            catch
            {
                //MessageBox.Show("Please try Again");
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show(Properties.Settings.Default.con);
                con.Open();
                currentDate = DateTime.Now.Date.ToString();
                filePresentFlag = createNewFile();
                timer.Interval = (5 * 1000); // 5 secs
                timer.Tick += new EventHandler(timer_Tick);
                timer.Start();
                using (StreamReader reader = new StreamReader(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\LocalFile.txt"))
                {
                    localfileDB = reader.ReadLine();
                }

                using (StreamReader reader = new StreamReader(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\ServerFile.txt"))
                {
                    serverfileDB = reader.ReadLine();
                    //copyServerfileDB = serverfileDB.Substring(serverfileDB.Length - 15, 15) + @"\BKP\ServerFile.txt";
                }


                if (filePresentFlag == true)
                {
                    _spManager.StartListening(portNameCollection);
                    this.ShowInTaskbar = false;
                    this.WindowState = FormWindowState.Minimized;
                }
                con.Close();
                //MessageBox.Show(localfileDB);
                //MessageBox.Show(serverfileDB);
            }
            catch(Exception ex)
            {
                con.Close();
                MessageBox.Show(ex.ToString());
            }
        }

        private void Form1_Resize(object sender, System.EventArgs e)
        {
            try
            {
                if (FormWindowState.Minimized == WindowState)
                    Hide();
            }
            catch
            {

            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Normal;
                this.Show();
            }
            catch
            {

            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Normal;
                this.Show();
            }
            catch
            {

            }
        }

        private void brwLocalFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog myFile = new OpenFileDialog();
                myFile.Filter = "Text File|*.txt";
                if (myFile.ShowDialog() == DialogResult.OK)
                {
                    localfileDB = myFile.FileName;
                }
                textBox1.Text = localfileDB;
            }
            catch
            {
                //MessageBox.Show("Please Try Again");
            }
        }

        private void brwServerFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog myFile = new OpenFileDialog();
                myFile.Filter = "Text File|*.txt";
                if (myFile.ShowDialog() == DialogResult.OK)
                {
                    serverfileDB = myFile.FileName;
                }
                textBox2.Text = serverfileDB;
            }
            catch
            {
                //MessageBox.Show("Please Try Again");
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string localFileName = (System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\LocalFile.txt");
                string tempfile = Path.GetTempFileName();
                using (var writer = new StreamWriter(tempfile))
                using (var reader = new StreamReader(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\LocalFile.txt"))
                {
                    writer.WriteLine(localfileDB);
                }
                File.Copy(tempfile, localFileName, true);

                string serverFileName = (System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\ServerFile.txt");
                tempfile = Path.GetTempFileName();

                using (var writer = new StreamWriter(tempfile))
                using (var reader = new StreamReader(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\ServerFile.txt"))
                {
                    writer.WriteLine(serverfileDB);
                }
                File.Copy(tempfile, serverFileName, true);

                MessageBox.Show("Changes Saved");
            }
            catch
            {
                //MessageBox.Show(ex.ToString());
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Clear();
                textBox2.Clear();
                textBox1.Focus();
            }
            catch
            {

            }
        }
        
        public void Maximiz()
        {
            try
            {
                this.WindowState = FormWindowState.Normal;
            }
            catch
            {

            }
        }
        private bool createNewFile()
        {
            DateTime savingDate = new DateTime();
            savingDate = DateTime.Now;
            string datestring = savingDate.ToString("yyyyMMdd");

            datestring = datestring + "_TMML_PPC.txt";

            using (StreamReader reader = new StreamReader(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\LocalFile.txt"))
            {
                localfileDB = reader.ReadLine();
            }
            if (String.IsNullOrEmpty(localfileDB))
            {
                filePresentFlag = false;
            }
            else
            {
                string localFileName = (System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\LocalFile.txt");
                string trimLocalFolder = localfileDB.Substring((localfileDB.Length - 21), 21);
                if (trimLocalFolder == datestring)
                {
                    if (File.Exists(localfileDB))
                    {
                        string tempfile = Path.GetTempFileName();
                        using (var writer = new StreamWriter(tempfile))
                            writer.WriteLine(localfileDB);
                        File.Copy(tempfile, localFileName, true);
                    }
                    else
                    {
                        System.IO.FileStream fs = File.Create(localfileDB);
                        fs.Close();
                        string tempfile = Path.GetTempFileName();
                        using (var writer = new StreamWriter(tempfile))
                            writer.WriteLine(localfileDB);
                        File.Copy(tempfile, localFileName, true);
                    }
                }
                else
                {
                    int posA = localfileDB.IndexOf(@"\2018");
                    trimLocalFolder = localfileDB.Substring(0, posA);
                    trimLocalFolder = trimLocalFolder + @"\" + datestring;
                    //File.Create(trimLocalFolder);
                    System.IO.FileStream fs = File.Create(trimLocalFolder);
                    fs.Close();
                    string tempfile = Path.GetTempFileName();
                    using (var writer = new StreamWriter(tempfile))
                        writer.WriteLine(trimLocalFolder);
                    File.Copy(tempfile, localFileName, true);
                }
                filePresentFlag = true;
            }

            using (StreamReader reader = new StreamReader(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\ServerFile.txt"))
            {
                serverfileDB = reader.ReadLine();
                //MessageBox.Show(serverfileDB);
            }
            if (String.IsNullOrEmpty(serverfileDB))
            {
                filePresentFlag = false;
            }
            else
            {
                string serverFileName = (System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\ServerFile.txt");

                //MessageBox.Show(serverFileName);

                string trimServerFolder = serverfileDB.Substring((serverfileDB.Length - 21), 21);
                string mainTrimFolderName = serverfileDB.Substring(0, serverfileDB.Length - 21);
                //mainTrimFolderName =
                //MessageBox.Show(trimServerFolder);

                if (trimServerFolder == datestring)
                {
                    if (File.Exists(serverfileDB))
                    {
                        string tempfile = Path.GetTempFileName();
                        using (var writer = new StreamWriter(tempfile))
                            writer.WriteLine(serverfileDB);
                        File.Copy(tempfile, serverFileName, true);
                        //MessageBox.Show(serverFileName);
                    }
                    else
                    {
                        System.IO.FileStream fs = File.Create(serverfileDB);
                        fs.Close();
                        System.IO.FileStream fs1 = File.Create(serverfileDB);
                        fs.Close();
                        string tempfile = Path.GetTempFileName();
                        using (var writer = new StreamWriter(tempfile))
                            writer.WriteLine(serverfileDB);
                        File.Copy(tempfile, serverFileName, true);
                        //MessageBox.Show(serverFileName);
                    }
                }
                else
                {
                    //string mainTrimFolderName;
                    int posA = serverfileDB.IndexOf(@"\2018");
                    //MessageBox.Show(posA.ToString());
                    trimServerFolder = serverfileDB.Substring(0, posA);
                    mainTrimFolderName = trimServerFolder;
                    trimServerFolder = trimServerFolder + @"\" + datestring;
                    //File.Create(trimServerFolder);
                    System.IO.FileStream fs = File.Create(trimServerFolder);
                    fs.Close();
                    //MessageBox.Show(trimServerFolder);

                    trimServerFolder = mainTrimFolderName + @"\BKP\" + datestring;
                    //File.Create(trimServerFolder);
                    System.IO.FileStream fs1 = File.Create(trimServerFolder);
                    fs1.Close();


                    string tempfile = Path.GetTempFileName();
                    using (var writer = new StreamWriter(tempfile))
                        writer.WriteLine(trimServerFolder);
                    File.Copy(tempfile, serverFileName, true);
                    //MessageBox.Show(serverFileName);
                }
                filePresentFlag = true;

            }
            using (StreamReader reader = new StreamReader(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\LocalFile.txt"))
            {
                localfileDB = reader.ReadLine();
            }

            using (StreamReader reader = new StreamReader(System.IO.Path.GetDirectoryName(Application.ExecutablePath) + @"\ServerFile.txt"))
            {
                serverfileDB = reader.ReadLine();
            }
            return filePresentFlag;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                saveToTxt();
                if (currentDate != DateTime.Now.Date.ToString())
                {
                    currentDate = DateTime.Now.Date.ToString();
                    createNewFile();
                }
            }
            catch
            {
                //MessageBox.Show("Please try Again");
            }
        }
        private void saveToTxt()
        {
            try
            {
                string servercontent = File.ReadAllText(serverfileDB);
                string localcontent = File.ReadAllText(localfileDB);
                //MessageBox.Show(localcontent);
                con.Close();
                con.Open();
                string q = "SELECT * FROM master where date = '" + DateTime.Now.ToString("yyyy-MM-dd") + "'";
                MySqlCommand command = new MySqlCommand(q, con);
                MySqlDataReader mdr = command.ExecuteReader();
                while (mdr.Read())
                {
                    if (servercontent.Contains(mdr.GetString(1)))
                    {
                        //MessageBox.Show("In If");
                    }
                    else
                    {
                        string tempfile = Path.GetTempFileName();
                        using (var writer = new StreamWriter(tempfile))
                        using (var reader = new StreamReader(serverfileDB))
                        {
                            writer.WriteLine(mdr.GetString(1));
                            while (!reader.EndOfStream)
                                writer.WriteLine(reader.ReadLine());
                            writer.Flush();
                            writer.Dispose();
                        }
                        File.Copy(tempfile, serverfileDB, true);
                    }
                    if (localcontent.Contains(mdr.GetString(1)))
                    {

                    }
                    else
                    {
                        string tempfile = Path.GetTempFileName();
                        using (var writer = new StreamWriter(tempfile))
                        using (var reader = new StreamReader(localfileDB))
                        {
                            writer.WriteLine(mdr.GetString(1));
                            while (!reader.EndOfStream)
                                writer.WriteLine(reader.ReadLine());
                            writer.Flush();
                            writer.Dispose();
                        }
                        File.Copy(tempfile, localfileDB, true);
                    }
                }
                con.Close();
            }
            catch
            {
                //MessageBox.Show(ex.ToString());
            }
        }
    }
}
